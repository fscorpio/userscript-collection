// ==UserScript==
// @name         wikimedia_highlight_language
// @namespace    http://florian.lahaye.me/
// @version      0.1.4
// @description  Highlight the language chosen by the user on sites of Wikimedia
// @author       Florian LAHAYE <florian@lahaye.me>
// @require      https://cdnjs.cloudflare.com/ajax/libs/coffee-script/1.1.2/coffee-script.min.js
// @match        http*://*.wikipedia.org/*
// @match        http*://*.wiktionary.org/*
// @match        http*://*.wikimedia.org/*
// @match        http*://*.wikiquote.org/*
// @match        http*://*.wikibooks.org/*
// @match        http*://wikisource.org/*
// @match        http*://*.wikisource.org/*
// @match        http*://*.wikinews.org/*
// @match        http*://*.wikiversity.org/*
// @match        http*://*.mediawiki.org/*
// @match        http*://*.wikivoyage.org/*
// @grant        none
// ==/UserScript==
/* jshint ignore:start */
var inline_src = (<><![CDATA[

    languages = ['en']
    #languages = ['fr', 'en']

    selectItems = (lang) ->
        url = window.location.href
        lang = '"' + lang + '"'

        document.querySelectorAll(
            if /^https?:\/\/www\.wikiquote\.org\/?$/.test(url)
            then "div.divArticleTier a[lang=#{lang}], div[lang=#{lang}] strong a"

            else if /^https?:\/\/www\.wikibooks\.org\/?$/.test(url)
            then "div.wm-project-list:not(.wm-project-list-other) a[lang=#{lang}], div[lang=#{lang}] strong"

            else if /^https?:\/\/www\.wikiversity\.org\/?$/.test(url)
            then "div.wm-project-list a[lang=#{lang}], div[lang=#{lang}] strong"

            else if /^https?:\/\/www\.[a-z]+\.org\/?$/.test(url)
            then "div.langlist:not(.langlist-other) a[lang=#{lang}], div[lang=#{lang}] strong"

            else "li a[lang=#{lang}]"
        )

    highlightItems = (items) ->
        for item in items
            item.style.color = 'green'
            item.style.fontWeight = 'bold'
        null

    for lang in languages
        highlightItems selectItems lang

]]></>).toString();
var compiled = this.CoffeeScript.compile(inline_src);
eval(compiled);
/* jshint ignore:end */
// vim: set ft=javascript ts=4 sw=4 et:
