# vim: set ft=coffee ts=4 sw=4 et:
'use strict'

normalSearch = document.querySelector('div#page-body form')
modsSearch = document.createElement('div')
normalSearch.parentElement.insertBefore(modsSearch, normalSearch)

modsSearch.outerHTML = '''
<form method="get" action="./search.php">
    <input type ="hidden" name="terms" value="all" />
    <input type ="hidden" name="fid[]" value="11" />
    <input type ="hidden" name="sc" value="1" />
    <input type ="hidden" name="sf" value="titleonly" />
    <input type ="hidden" name="sr" value="topics" />
    <input type ="hidden" name="sk" value="t" />
    <input type ="hidden" name="sd" value="d" />
    <input type ="hidden" name="st" value="0" />
    <input type ="hidden" name="ch" value="300" />
    <input type ="hidden" name="t" value="0" />

    <div class="topic-actions">
        <div class="search-box">
            <label for="keywords_mods">Search mod: <input type="text" name="keywords" id="keywords_mods" value="" class="inputbox narrow"></label>
            <input class="button2" type="submit" name="submit" value="Search" />
        </div>
    </div>
</form>
'''
