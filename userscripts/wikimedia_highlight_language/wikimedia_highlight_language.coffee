# vim: set ft=coffee ts=4 sw=4 et:
'use strict'

languages = ['en']
#languages = ['fr', 'en']

selectItems = (lang) ->
    url = window.location.href
    lang = '"' + lang + '"'

    document.querySelectorAll(
        if /^https?:\/\/www\.wikiquote\.org\/?$/.test(url)
        then "div.divArticleTier a[lang=#{lang}], div[lang=#{lang}] strong a"

        else if /^https?:\/\/www\.wikibooks\.org\/?$/.test(url)
        then "div.wm-project-list:not(.wm-project-list-other) a[lang=#{lang}], div[lang=#{lang}] strong"

        else if /^https?:\/\/www\.wikiversity\.org\/?$/.test(url)
        then "div.wm-project-list a[lang=#{lang}], div[lang=#{lang}] strong"

        else if /^https?:\/\/www\.[a-z]+\.org\/?$/.test(url)
        then "div.langlist:not(.langlist-other) a[lang=#{lang}], div[lang=#{lang}] strong"

        else "li a[lang=#{lang}]"
    )

highlightItems = (items) ->
    for item in items
        item.style.color = 'green'
        item.style.fontWeight = 'bold'
    null

for lang in languages
    highlightItems selectItems lang
