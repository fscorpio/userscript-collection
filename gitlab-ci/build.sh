#!/bin/bash
set -e

cd userscripts
for us in $(echo *)
do
    cd ${us}
    make
    cd ..
done
cd ..

mkdir -p build
cp userscripts/*/*.user.js build
