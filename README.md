# README #

# **Moved to https://gitlab.com/fscorpio/userscript-collection** #

For Tampermonkey extension.  
To add a script, use one of the following URL.

## URLs ##

To add a search bar for mods on the official Minetest forum  
-> https://bitbucket.org/fscorpio/userscript-collection/raw/master/mod_search_on_minetest_forum.user.js

To highlight the language chosen by the user on sites of Wikimedia (currently english only)  
-> https://bitbucket.org/fscorpio/userscript-collection/raw/master/wikimedia_highlight_language.user.js