# README #

For Tampermonkey extension.  
To add a script, use one of the following URL.

## URLs ##

To add a search bar for mods on the official Minetest forum  
-> https://fscorpio.gitlab.io/userscript-collection/mod_search_on_minetest_forum.user.js

To highlight the language chosen by the user on sites of Wikimedia (currently english only)  
-> https://fscorpio.gitlab.io/userscript-collection/wikimedia_highlight_language.user.js
